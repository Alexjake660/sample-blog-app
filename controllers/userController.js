var User = require('../models/user');
var models = require('../models');

// Display user create form on GET.
exports.user_create_get = function(req, res, next) {
        // create user GET controller logic here 
        res.render('forms/user_form', { title: 'Create User GET',  layout: 'layouts/detail'});
};

// Handle user create on POST.
exports.user_create_post = function(req, res, next) {
     // create user POST controller logic here
     // create a new post based on the fields in our user model
     models.User.create({
      first_name: req.body.first_name,
        last_name: req.body.last_name,
        username : req.body.username,
        role : req.body.role,
        email : req.body.email
    }).then(function() {
        console.log("User created successfully");
       // check if there was an error during post creation
       // If a user gets created successfully, we just redirect to users list
       // no need to render a page
       res.redirect("/blog/users");
    }).catch(function(err) {
        // print the error details
        console.log(err, request.body);
    }
    )};

// Display user delete form on GET.
exports.user_delete_get = function(req, res, next) {
        // GET logic to delete a user here
        models.User.destroy({
                // find the user_id to delete from database
                where: {
                  id: req.params.user_id
                }
              }).then(function() {
               // If an post gets deleted successfully, we just redirect to posts list
               // no need to render a page
                res.redirect('/blog/users');
                console.log("User deleted successfully");
              });
        // renders user delete page
        // res.redirect("/blog/user");
        // res.render('pages/user_delete', { title: 'GET function to delete user',  layout: 'layouts/detail'} );
};

// // Handle user delete on POST.
// exports.user_delete_post = function(req, res, next) {
//         // POST logic to delete an user here
//         // If an user gets deleted successfully, we just redirect to users list
//         // no need to render a page
//         res.redirect("/blog/user");
//         // res.render('pages/user_delete', { title: 'POST function to delete user',  layout: 'layouts/detail'} );
// };

// Display user update form on GET.
exports.user_update_get = function(req, res, next) {
        // GET logic to update an user here
        console.log("ID is " + req.params.user_id);
        models.User.findById(
                req.params.user_id
        ).then(function(user) {
               // renders user form
               res.render('forms/user_form', { title: 'Update User', user: user, layout: 'layouts/detail'});
               console.log("User update get successful");
          });
        
};

// Handle post update on POST.
exports.user_update_post = function(req, res, next) {
        // POST logic to update an user here
        console.log("ID is " + req.params.user_id);
        models.User.update(
        // Values to update
            {
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                username : req.body.username,
                role : req.body.role,
                email : req.body.email
            },
          { // Clause
                where: 
                {
                    id: req.params.user_id
                }
            }
        //   returning: true, where: {id: req.params.user_id} 
         ).then(function() { 
                // If a user gets updated successfully, we just redirect to posts list
                // no need to render a page
                res.redirect("/blog/users");  
                console.log("User updated successfully");
         });
};

// Display list of all users.
exports.user_list = function(req, res, next) {
        // GET controller logic to list all users
        models.User.findAll(
        ).then(function(users) {
        console.log("rendering users list");
        // renders all users list
        res.render('pages/user_list', { title: 'User List', users: users,  layout: 'layouts/list'} );
        console.log("Users list renders successfully");
        });       
};

// Display detail page for a specific user.
exports.user_detail = function(req, res, next) {
        // GET controller logic to display just one user
        // find a post by the primary key Pk
        models.User.findById(
                req.params.user_id
        ).then(function(user) {
                console.log("Database returns user ", user.id);
                console.log("Database returns user ", user.first_name);
                console.log("Database returns user ", user.last_name);
                console.log("Database returns user ", user.username);
                // renders an inividual post details page
                res.render('pages/user_detail', { title: 'User Details', user: user, layout: 'layouts/detail'} );
                console.log("User details renders successfully");
        });
};

// exports.index = function(req, res) {
//         // So, render user index page
//         res.render('pages/user_index', { title: 'User Homepage',  layout: 'layouts/main'} );
// };

// exports.user_all_route = function(req, res) {
//         // So, redirect to user index page
//          res.redirect("/blog/users");
// };
 